namespace :magazine do

  desc "Load dummy data to test"
  task :add_dummy_data => :environment do
    
    ## User dummy data...
    (0..5).each do |i|
      user = User.create(name: "User #{i}", email: "dummy.user#{i}@xmail.com", password: "foobar", password_confirmation: "foobar")
      puts "User #{user.name} successfully created."  
    end

    ## Magazine dummy data...
    (0..11).each do |i|
      magazine = Magazine.create(name: "Magazine#{i}", published_by: "Publication No #{i}")
      puts "Magazine #{magazine.name} successfully created."

      ## Magazine Articles
      (0..7).each do |i|
        article = magazine.articles.create(title: "Article #{i}", author: "Author #{i}", body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,")
        puts "Article #{article.title} successfully created."
        User.all.each do |user|
          comment = Comment.build_from( article, user.id, "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source." ).save
        end  
      end 
    end  

  end

end