class Article < ActiveRecord::Base
  belongs_to :magazine
  has_many :comments, :as => :commentable

  acts_as_commentable
end
