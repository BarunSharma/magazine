class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate

  helper_method :current_user

  def current_user
    @current_user ||= User.find_by_email!(session[:email]) if session[:email]
  rescue
    @current_user = nil
  end

  def authenticate
    if current_user.blank?
      redirect_to root_path, :flash => {:error => "Not Authorized"}
    end
  end

end
