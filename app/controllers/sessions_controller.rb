class SessionsController < ApplicationController

	skip_before_action :authenticate

	def new
	end	

	def create
		auth_hash = request.env['omniauth.auth']
		session[:email] = auth_hash['info']['email']
		if session[:email].present?
			redirect_to magazines_path, :flash => {:success => "Successfully Signed In.."}
		else
			redirect_to root_path, :flash => {:error => params[:message]}
		end	
	end	

	def failure
	    redirect_to request.referer, :flash => {:error => params[:message]}
  	end

	def destroy
	    session[:email] = nil
	    redirect_to root_path, :flash => {:success => 'User successfully logged out.'}
	end

	
end