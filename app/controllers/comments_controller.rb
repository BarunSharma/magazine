class CommentsController < ApplicationController

	def create
		article = Article.where(id: params[:article_id]).first
		comment = Comment.build_from( article, current_user.id, params[:comment])
		comment.save
		redirect_to magazine_article_path(magazine_id: article.magazine_id, id: article.id), flash: {notice: "Comment successfully posted..."}
	end	

	def child_comment
		comment = Comment.where(id: params[:id]).first
		article = comment.commentable
		child_comment = Comment.build_from( article, current_user.id, params[:comment])
		child_comment.save
		child_comment.move_to_child_of(comment)
		redirect_to magazine_article_path(magazine_id: article.magazine_id, id: article.id), flash: {notice: "Comment successfully posted..."}
	end	

	def destroy
		comment = Comment.where(id: params[:id]).first
		article = comment.commentable
		comment.destroy
		redirect_to magazine_article_path(magazine_id: article.magazine_id, id: article.id), flash: {notice: "Comment successfully deleted..."}
	end	
	
end