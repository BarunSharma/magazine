class MagazinesController < ApplicationController

	def index
		@magazines = Magazine.includes( articles: [:comments]).all
	end	

	def show
		@magazine = Magazine.where(id: params[:id]).first
	end	
	
end